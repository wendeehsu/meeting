# Meeting

## Install Libraries
```
pip3 install -r requirements.txt
```

## Execute
```
python3 meeting.py
```

It will automatically create a meeting document named after today's date such as `2020-02-08` in the meeting drive.
