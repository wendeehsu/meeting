from Google import Create_Service
from datetime import datetime, timedelta 
import os

# authenticate
client_secret_file = 'credentials.json'
api_name = 'drive'
api_version = 'v3'
scopes = ['https://www.googleapis.com/auth/drive']
service = Create_Service(client_secret_file, api_name, api_version, scopes)
date = datetime.now() + timedelta(days=1)
date = date.strftime("%Y/%m/%d")

# ls folders
folder_id = ['1b9Xb1ZYT7fWWmh8-itVZv52ZSEPxlXnP'] # meeting folder's id

# copy template file
template_file_id = '12-5N1gVF9oKmg2pLMCiP-iuz2EmXTmGTuyCIoKIDnjw'
file_metadata = {
    'name': date,
    'parents': folder_id,
    'description': 'dentall weekly meeting: ' + date
}

service.files().copy(
    fileId=template_file_id,
    body=file_metadata
).execute()

query = f"'{folder_id[0]}' in parents"
response = service.files().list(q=query).execute()
files = response.get('files')
# print(files)

file_id = None
for file in files:
    if file["name"] == date:
        file_id = file["id"]
        break

if file_id is not None:
    filename = "https://docs.google.com/document/d/" + file_id + "/edit?usp=sharing"
    print(date + " meeting: " + filename)
